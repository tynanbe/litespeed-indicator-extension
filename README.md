# LiteSpeed Indicator Browser Extension

This is a Browser Extension to quickly reference if a page was served from
LiteSpeed cache.

The extension has the following states:

<img src="img/grey.png" width="16"> LiteSpeed could not be found.

<img src="img/blue.png" width="16"> LiteSpeed is present.

<img src="img/green.png" width="16"> Cache hit.

<img src="img/red.png" width="16"> Cache miss.

## Headers

The extension scans response headers. If `server` is `LiteSpeed` or an
`x-litespeed*` header is found, it marks LiteSpeed as present.

If `x-litespeed-cache` is `hit`, it marks LiteSpeed as hit.

If `x-litespeed-cache` is `miss`, it marks LiteSpeed as miss.
