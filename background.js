const button = {
  active: false,
  status: null,
};

const buttonActive = {
  [true]: "blue",
  [false]: "grey",
};

const buttonStatus = {
  hit: "green",
  miss: "red",
};

function isLiteSpeedPresent({ headers = [] } = {}) {
  return headers.some(({ name, value }) => {
    return (
      ("server" === name && "LiteSpeed" === value) ||
      name.startsWith("x-litespeed")
    );
  });
}

function liteSpeedStatus({ headers = [] } = {}) {
  headers = headers.reverse();

  const hitMissHeader = headers.find(
    ({ name }) => name === "x-litespeed-cache",
  );

  if (hitMissHeader) {
    return hitMissHeader.value.toLowerCase();
  }

  return null;
}

chrome.webRequest.onHeadersReceived.addListener(
  (details) => {
    if (details.type === "main_frame") {
      const headers = details.responseHeaders;
      button.active = isLiteSpeedPresent({ headers });
      button.status = liteSpeedStatus({ headers });
    }
  },
  {
    urls: ["http://*/*", "https://*/*"],
  },
  ["responseHeaders"],
);

chrome.webNavigation.onCompleted.addListener((details) => {
  if (details.frameId === 0) {
    chrome.pageAction.setIcon({
      path: `img/${
        (button.status && buttonStatus[button.status]) ||
        buttonActive[button.active]
      }.png`,
      tabId: details.tabId,
    });
  }
});
